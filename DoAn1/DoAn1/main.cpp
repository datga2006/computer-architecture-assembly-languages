#include <iostream>
#include <bitset>
#include "QInt.h"
#include <fstream>

#define basi sizeof(BasisType)
#define lenn 128/8/basi
using namespace std;
string LoadLine(ifstream& f);
string ProcessCommand(int arg, char *data[]);

void main(int args, char *data[])
{
	string tmp;
	if (args == 3)
	{
		ifstream inp(data[1]);
		bool XuongDong = 0;
		if (inp.is_open())
		{
			ofstream out(data[2]);
			while (!inp.eof())
			{
				if (XuongDong) out << endl;
				tmp = LoadLine(inp);
				out << tmp;
				XuongDong = 1;
			}
		}
	}
	if (args > 3)
	{
		cout << ProcessCommand(args, data);
	}
}

string LoadLine(ifstream& f)
{
	QInt a;
	QInt b;
	QInt c;
	string tmp;
	f >> tmp;
	int CoSo1 = 0;
	int CoSo2 = 0;
	if (tmp == "2")CoSo1 = 2;
	if (tmp == "10")CoSo1 = 10;
	if (tmp == "16")CoSo1 = 16;
	f >> tmp;
	if (tmp == "2")CoSo2 = 2;
	if (tmp == "10")CoSo2 = 10;
	if (tmp == "16")CoSo2 = 16;
	if (CoSo2 > 0)
	{
		f >> tmp;
		switch (CoSo1)
		{
		case 2:
			a.FromBin((char*)tmp.c_str());
			break;
		case 10:
			a.FromDec((char*)tmp.c_str());
			break;
		case 16:
			a.FromHex((char*)tmp.c_str());
			break;
		default:
			break;
		}
		switch (CoSo2)
		{
		case 2:
			return a.ToBin(false);
			break;
		case 10:
			return a.ToDec();
			break;
		case 16:
			return a.ToHex();
			break;
		default:
			break;
		}
	}
	if (tmp == "ror")
	{
		f >> tmp;
		switch (CoSo1)
		{
		case 2:
			a.FromBin((char*)tmp.c_str());
			a.RolloverRight();
			return a.ToBin(false);
			break;
		case 10:
			a.FromDec((char*)tmp.c_str());
			a.RolloverRight();
			return a.ToDec();
			break;
		case 16:
			a.FromHex((char*)tmp.c_str());
			a.RolloverRight();
			return a.ToHex();
			break;
		default:
			break;
		}
	}
	if (tmp == "rol")
	{
		f >> tmp;
		switch (CoSo1)
		{
		case 2:
			a.FromBin((char*)tmp.c_str());
			a.RolloverLeft();
			return a.ToBin(false);
			break;
		case 10:
			a.FromDec((char*)tmp.c_str());
			a.RolloverLeft();
			return a.ToDec();
			break;
		case 16:
			a.FromHex((char*)tmp.c_str());
			a.RolloverLeft();
			return a.ToHex();
			break;
		default:
			break;
		}
	}
	if (tmp == "~")
	{
		f >> tmp;
		switch (CoSo1)
		{
		case 2:
			a.FromBin((char*)tmp.c_str());
			a = ~a;
			return a.ToBin(false);
			break;
		case 10:
			a.FromDec((char*)tmp.c_str());
			a = ~a;
			return a.ToDec();
			break;
		case 16:
			a.FromHex((char*)tmp.c_str());
			a = ~a;
			return a.ToHex();
			break;
		default:
			break;
		}
	}
	switch (CoSo1)
	{
	case 2:
		a.FromBin((char*)tmp.c_str());
		break;
	case 10:
		a.FromDec((char*)tmp.c_str());
		break;
	case 16:
		a.FromHex((char*)tmp.c_str());
		break;
	default:
		break;
	}
	string tmp2;
	f >> tmp2;
	f >> tmp;
	switch (CoSo1)
	{
	case 2:
		b.FromBin((char*)tmp.c_str());
		break;
	case 10:
		b.FromDec((char*)tmp.c_str());
		break;
	case 16:
		b.FromHex((char*)tmp.c_str());
		break;
	default:
		break;
	}
	if (tmp2 == "+")
	{
		c = a + b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "-")
	{
		c = a - b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "*")
	{
		c = a * b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "/")
	{
		c = a / b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "%")
	{
		c = a % b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "|")
	{
		c = a | b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "&")
	{
		c = a & b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "^")
	{
		c = a ^ b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == ">>")
	{
		c = a >> b.GetStorage()[lenn - 1];
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "<<")
	{
		c = a << b.GetStorage()[lenn - 1];
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	return "";
}
string ProcessCommand(int arg, char *data[])
{
	QInt a;
	QInt b;
	QInt c;
	int dem = 1;
	string tmp;
	tmp = data[dem];
	dem++;
	int CoSo1 = 0;
	int CoSo2 = 0;
	if (tmp == "2")CoSo1 = 2;
	if (tmp == "10")CoSo1 = 10;
	if (tmp == "16")CoSo1 = 16;
	tmp = data[dem];
	dem++;
	if (tmp == "2")CoSo2 = 2;
	if (tmp == "10")CoSo2 = 10;
	if (tmp == "16")CoSo2 = 16;
	if (CoSo2 > 0 && arg == 4)
	{
		tmp = data[dem];
		dem++;
		switch (CoSo1)
		{
		case 2:
			a.FromBin((char*)tmp.c_str());
			break;
		case 10:
			a.FromDec((char*)tmp.c_str());
			break;
		case 16:
			a.FromHex((char*)tmp.c_str());
			break;
		default:
			break;
		}
		switch (CoSo2)
		{
		case 2:
			return a.ToBin(false);
			break;
		case 10:
			return a.ToDec();
			break;
		case 16:
			return a.ToHex();
			break;
		default:
			break;
		}
	}
	if (tmp == "ror")
	{
		tmp = data[dem];
		dem++;
		switch (CoSo1)
		{
		case 2:
			a.FromBin((char*)tmp.c_str());
			a.RolloverRight();
			return a.ToBin(false);
			break;
		case 10:
			a.FromDec((char*)tmp.c_str());
			a.RolloverRight();
			return a.ToDec();
			break;
		case 16:
			a.FromHex((char*)tmp.c_str());
			a.RolloverRight();
			return a.ToHex();
			break;
		default:
			break;
		}
	}
	if (tmp == "rol")
	{
		tmp = data[dem];
		dem++;
		switch (CoSo1)
		{
		case 2:
			a.FromBin((char*)tmp.c_str());
			a.RolloverLeft();
			return a.ToBin(false);
			break;
		case 10:
			a.FromDec((char*)tmp.c_str());
			a.RolloverLeft();
			return a.ToDec();
			break;
		case 16:
			a.FromHex((char*)tmp.c_str());
			a.RolloverLeft();
			return a.ToHex();
			break;
		default:
			break;
		}
	}
	if (tmp == "~")
	{
		tmp = data[dem];
		dem++;
		switch (CoSo1)
		{
		case 2:
			a.FromBin((char*)tmp.c_str());
			a = ~a;
			return a.ToBin(false);
			break;
		case 10:
			a.FromDec((char*)tmp.c_str());
			a = ~a;
			return a.ToDec();
			break;
		case 16:
			a.FromHex((char*)tmp.c_str());
			a = ~a;
			return a.ToHex();
			break;
		default:
			break;
		}
	}
	switch (CoSo1)
	{
	case 2:
		a.FromBin((char*)tmp.c_str());
		break;
	case 10:
		a.FromDec((char*)tmp.c_str());
		break;
	case 16:
		a.FromHex((char*)tmp.c_str());
		break;
	default:
		break;
	}
	string tmp2;
	tmp2 = data[dem];
	dem++;
	tmp = data[dem];
	dem++;
	switch (CoSo1)
	{
	case 2:
		b.FromBin((char*)tmp.c_str());
		break;
	case 10:
		b.FromDec((char*)tmp.c_str());
		break;
	case 16:
		b.FromHex((char*)tmp.c_str());
		break;
	default:
		break;
	}
	if (tmp2 == "+")
	{
		c = a + b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "-")
	{
		c = a - b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "*")
	{
		c = a * b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "/")
	{
		c = a / b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "%")
	{
		c = a % b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "|")
	{
		c = a | b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "&")
	{
		c = a & b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "^")
	{
		c = a ^ b;
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == ">>")
	{
		c = a >> b.GetStorage()[lenn - 1];
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	if (tmp2 == "<<")
	{
		c = a << b.GetStorage()[lenn - 1];
		switch (CoSo1)
		{
		case 2:
			return c.ToBin(false);
		case 10:
			return c.ToDec();
		case 16:
			return c.ToHex();
		default:
			break;
		}
	}
	return "";
}