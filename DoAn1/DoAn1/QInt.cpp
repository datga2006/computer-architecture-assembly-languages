#include "QInt.h"
#include <string>
#include <iostream>
QInt::QInt()
{
	//khoi tao vung nho voi do dai _length
	_storage = new BasisType[_length];
	//gan cac phan tu trong vung nho bang 0
	for (int i = 0; i < _length; i++)
	{
		_storage[i] = 0;
	}
}

QInt::QInt(BasisType * __a)
{
	_storage = __a;
}


QInt::~QInt()
{
	if (_storage) delete[]_storage;
}

BasisType * QInt::GetStorage()
{
	return _storage;
}

bool QInt::IsLessThanZero()
{
	//neu bit dau tien cua phan tu dau tien cua vung nho bang 1 thi tra ve true
	if (_storage[0] >> (_basisSize * 8 - 1)) return true;
	return false;
}

QInt QInt::operator|(const QInt & __a)
{
	//thuc hien phep or tren tung phan tu cua mang, luu vao bien tmp, tra ve ket qua
	BasisType *tmp = new BasisType[_length];
	for (int i = 0; i < _length; i++)
	{
		tmp[i] = _storage[i] | __a._storage[i];
	}
	return QInt(tmp);
}

QInt QInt::operator&(const QInt & __a)
{
	//thuc hien phep and tren tung phan tu cua mang, luu vao bien tmp, tra ve ket qua
	BasisType *tmp = new BasisType[_length];
	for (int i = 0; i < _length; i++)
	{
		tmp[i] = _storage[i] & __a._storage[i];
	}
	return QInt(tmp);
}

QInt QInt::operator^(const QInt & __a)
{
	//thuc hien phep xor tren tung phan tu cua mang, luu vao bien tmp, tra ve ket qua
	BasisType *tmp = new BasisType[_length];
	for (int i = 0; i < _length; i++)
	{
		tmp[i] = _storage[i] ^ __a._storage[i];
	}
	return QInt(tmp);
}

QInt QInt::operator!()
{
	//thuc hien phep ! tren tung phan tu cua mang, luu vao bien tmp, tra ve ket qua
	BasisType *tmp = new BasisType[_length];
	for (int i = 0; i < _length; i++)
	{
		tmp[i] = !_storage[i];
	}
	return QInt(tmp);
}

QInt QInt::operator~()
{
	//thuc hien phep not tren tung phan tu cua mang, luu vao bien tmp, tra ve ket qua
	BasisType *tmp = new BasisType[_length];
	for (int i = 0; i < _length; i++)
	{
		tmp[i] = ~_storage[i];
	}
	return QInt(tmp);
}

QInt QInt::operator<<(BasisType __a)
{
	//sao chep vung nho cua __a vao tmp
	BasisType *tmp = new BasisType[_length];
	for (int i = 0; i < _length; i++)
	{
		tmp[i] = _storage[i];
	}
	//thuc hien lap lai viec dich 1 bit sang trai __a lan
	//trong moi vong lap, dich cac phan tu sang trai 1 bit
	//neu phan tu phia sau co bit dau tien bang 1 thi cong 1 vao phan tu hien tai
	for (int j = 0; j < __a; j++)
	{
		for (int i = 0; i < _length; i++)
		{
			tmp[i] = tmp[i] << 1;
			if (i + 1 < _length)
			{
				BasisType tmp2 = tmp[i + 1];
				tmp2 = tmp2 >> (_basisSize * 8 - 1);
				tmp[i] = tmp[i] | tmp2;
			}
		}
	}
	
	return QInt(tmp);
}

QInt QInt::operator >> (BasisType __a)
{
	//sao chep vung nho cua __a vao tmp
	BasisType *tmp = new BasisType[_length];
	for (int i = 0; i < _length; i++)
	{
		tmp[i] = _storage[i];
	}
	//thuc hien lap lai viec dich 1 bit sang phai __a lan
	//trong moi vong lap, dich cac phan tu sang phai 1 bit
	//neu phan tu phia truoc co bit cuoi cung bang 1 thi cong vao phan bit dau tien cua phan tu hien tai
	for (int j = 0; j < __a; j++)
	{
		for (int i = _length - 1; i >= 0; i--)
		{
			tmp[i] = tmp[i] >> 1;
			if (i - 1 >= 0)
			{
				BasisType tmp2 = tmp[i - 1];
				tmp2 = tmp2 << (_basisSize * 8 - 1);
				tmp[i] = tmp[i] | tmp2;
			}
		}
	}
	
	return QInt(tmp);
}

QInt& QInt::operator=(QInt & __a)
{
	for (int i = 0; i < _length; i++)
	{
		_storage[i] = __a._storage[i];
	}
	return *this;
}

bool QInt::operator>(QInt & __a)
{
	//neu t1 > 0, t2 < 0 thi tra ve true va nguoc lai
	//voi t1 la phan tu thu nhat, t2 la phan tu thu 2
	bool t1 = IsLessThanZero();
	bool t2 = __a.IsLessThanZero();
	if (t1 && !t2) return false;
	if (!t1 && t2) return true;
	//neu t1 va t1 dau be hon 0 thi tra ve !(bu 2 cua t1 > bu 2 cua t2)
	if (t1&&t2) return !(TwosComplement() > __a.TwosComplement());
	//neu t1 va t2 lon hon 0 thi so sanh tu trai sang phai
	//neu co phan tu nao cua t1 > t2 thi tra ve true
	//neu co phan tu nao cua t1 < t2 thi tra ve false
	if (!t1 && !t2)
	{
		for (int i = 0; i < _length; i++)
		{
			if (_storage[i] < __a._storage[i]) return false;
			if (_storage[i] > __a._storage[i]) return true;
		}
	}
	return false;
}

bool QInt::operator>=(QInt & __a)
{
	if (*this > __a || *this == __a) return true;
	return false;
}

bool QInt::operator<(QInt & __a)
{
	return !(*this>=__a);
}

bool QInt::operator<=(QInt & __a)
{
	return !(*this > __a);
}

bool QInt::operator==(QInt & __a)
{
	for (int i = 0; i < _length; i++)
	{
		if (_storage[i] != __a._storage[i]) return false;
	}
	return true;
}

bool QInt::operator!=(QInt & __a)
{
	return !(*this == __a);
}

QInt QInt::TwosComplement()
{
	//sao chep vung nho vao tmp
	//tim bu 1
	BasisType* tmp = new BasisType[_length];
	for (int i = 0; i < _length; i++)
	{
		tmp[i] = ~_storage[i];
	}
	BasisType tmp2 = 0b0; //bien nho
	BasisType tmp3 = 0b1; //cong them 1 de tim bu 2
	BasisType prev;
	for (int i = _length-1; i >= 0; i--)
	{
		prev = tmp[i];
		tmp[i] = tmp[i] + tmp2 + tmp3;
		tmp3 = 0b0;
		if (tmp[i] < prev) tmp2 = 1;
		else tmp2 = 0;
	}
	return QInt(tmp);
}

QInt QInt::ShiftRightArithmetic(BasisType __a)
{
	//sao chep vung nho cua __a vao tmp
	BasisType *tmp = new BasisType[_length];
	for (int i = 0; i < _length; i++)
	{
		tmp[i] = _storage[i];
	}
	BasisType __signed = (tmp[0] >> (_basisSize*8 - 1)) << (_basisSize*8 - 1); //luu bit dau
	for (int j = 0; j < __a; j++)
	{
		for (int i = _length - 1; i >= 0; i--)
		{
			tmp[i] = tmp[i] >> 1;
			if (i - 1 >= 0)
			{
				BasisType tmp2 = tmp[i - 1];
				tmp2 = tmp2 << (_basisSize * 8 - 1);
				tmp[i] = tmp[i] | tmp2;
			}
		}
	}
	if (__a > 0) tmp[0] = tmp[0] + __signed;
	return QInt(tmp);
}

QInt QInt::ShiftLeftArithmetic(BasisType __a)
{
	return *this << __a;
}

void QInt::RolloverLeft()
{
	//dich sang trai 1 bit, neu bit dau tien la 1 thi cong vao bit cuoi cung 1
	BasisType tmp = ((1 << (_basisSize * 8 - 1))&_storage[0]) ? 1 : 0;
	*this = *this << 1;
	_storage[_length - 1] += tmp;
}

void QInt::RolloverRight()
{
	//dich sang phai 1 bit, neu bit cuoi cung la 1 thi cong vao bit dau tien 1
	BasisType tmp = (1&_storage[_length-1]) ? (1<<(_basisSize * 8 - 1)) : 0;
	*this = *this >> 1;
	_storage[0] += tmp;
}

QInt QInt::operator+(const QInt & __a)
{
	//tao vung nho tam
	//cong tu phai sang trai luu vao vung nho tam
	//neu phep cong tai 1 phan tu bi tran so thi nho 1, cong vao phan tu trong lan lap ke tiep
	BasisType* tmp = new BasisType[_length];
	BasisType tmp2 = 0b0;
	for (int i = _length-1; i >= 0; i--)
	{
		tmp[i] = _storage[i] + tmp2;
		tmp2 = 0b0;
		if (tmp[i] < _storage[i]) tmp2 = 0b1;
		tmp[i] = tmp[i] + __a._storage[i];
		if ((tmp[i] < _storage[i] || tmp[i] < __a._storage[i]))
			tmp2 = 0b1;
	}
	return QInt(tmp);
}

QInt QInt::operator-(QInt & __a)
{
	//lay bu 2, thuc hien phep cong
	return *this + __a.TwosComplement();
}

QInt QInt::operator*(QInt & __a)
{
	//neu bit thu i cua __a (tinh tu phai sang trai) bang 1 thi dich *this sang trai i bit
	//roi cong vao tmp
	QInt tmp;
	for (int i = 0; i < _length*_basisSize*8; i++)
	{
		if (__a.GetBitByPosition(i))
		{
			tmp = tmp + (*this << i);
		}
	}
	//Tao vung nho de tra ve ket qua
	BasisType *t = new BasisType[_length];
	for (int i = 0; i < _length; i++)
	{
		t[i] = tmp.GetStorage()[i];
	}
	return QInt(t);
}

QInt QInt::operator/(QInt & __a)
{
	//kiem tra dau cua 2 hang tu
	//neu hang tu nao be hon 0 thi lay bu 2
	//neu 2 hang tu khac dau thi bien rsSigned = 1 de xac dinh dau cua ket qua
	bool rsSigned = 0;
	QInt o1, o2;
	if (IsLessThanZero())
	{
		rsSigned = !rsSigned;
		o1 = TwosComplement();
	}
	else
	{
		o1 = *this;
	}
	if (__a.IsLessThanZero())
	{
		rsSigned = !rsSigned;
		o2 = __a.TwosComplement();
	}
	else
	{
		o2 = __a;
	}
	//dung bien rs de luu ket qua
	//neu o1 >= o2 thi o1=o1-o2
	//dem tang 1 don vi
	//tra ve ket qua la bien dem (rs)
	QInt rs;
	QInt one;
	one.FromBin("1");
	while (o1>=o2)
	{
		o1 = o1 - o2;
		rs = rs + one;
	}
	//khoi tao vung nho de tra ve ket qua
	//neu rsSigned = 1 thi tra ve bu 2 cua bien dem (rs)
	BasisType *tmp = new BasisType[_length];
	for (int i = 0; i < _length; i++)
	{
		tmp[i] = rs.GetStorage()[i];
	}
	if (rsSigned) return QInt(tmp).TwosComplement();
	else return QInt(tmp);
}
//tuong tu nhu phep chia tren nhung tra ve o1
QInt QInt::operator%(QInt & __a)
{
	QInt o1, o2;
	if (IsLessThanZero())
	{
		o1 = TwosComplement();
	}
	else
	{
		o1 = *this;
	}
	if (__a.IsLessThanZero())
	{
		o2 = __a.TwosComplement();
	}
	else
	{
		o2 = __a;
	}
	while (o1 >= o2)
	{
		o1 = o1 - o2;
	}
	if (IsLessThanZero()) o1 = o1.TwosComplement();
	BasisType *rs = new BasisType[_length]; //result
	for (int i = 0; i < _length; i++)
	{
		rs[i] = o1.GetStorage()[i];
	}
	return QInt(rs);
}

bool QInt::FromHex(char* s)
{
	//Dua du lieu chuoi s vao chuoi BigIntChar
	//Kiem tra chuoi hop le hay khong
	//chuyen dang ky tu thanh dang so
	int len = strlen(s);
	char BigIntChar[256];
	memcpy(BigIntChar, s, len);
	for (int i = 0; i < len; i++)
	{
		switch (BigIntChar[i])
		{
		case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
			BigIntChar[i] -= '0';
			break;
		case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
			BigIntChar[i] += -'A' + 10;
			break;
		case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
			BigIntChar[i] += -'a' + 10;
			break;
		default:
			return false;
		}
	}
	//Dua tung gia tri thap luc phan vao QInt.data sao cho dung vi tri
	for (int i = 0; i < _length; i++)
	{
		_storage[_length - 1 - i] = 0;
		len -= 2;
		for (int j = 0; j < 2; j++)
		{
			if (len + j < 0) continue;
			_storage[_length - 1 - i] = _length * _storage[_length - 1 - i] + BigIntChar[len + j];
		}
	}
	return true;
}

std::string QInt::ToHex()
{
	//Dua tung gia tri thap luc phan trong QInt.data vao chuoi s sao cho dung vi tri
	char *s = new char[33];
	int len = 32;
	s[len] = '\0';
	for (int i = 0; i < _length; i++)
	{
		int t = _storage[_length -1-i];
		if (t < 0) t += 256;
		len -= 2;
		for (int j = 1; j >= 0; j--)
		{
			s[len + j] = t % _length;
			t /= _length;
		}
	}
	//Chuyen nhung gia tri co nghia o cuoi chuoi len dau chuoi
	//Chuyen doi dang so thanh dang ky tu
	int p;
	for (p = 0; p < 31; p++)
		if (s[p] > 0) break;
	for (int i = p; i < 32; i++)
	{
		switch (s[i])
		{
		case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9:
			s[i - p] = s[i] + '0';
			break;
		case 10: case 11: case 12: case 13: case 14: case 15:
			s[i - p] = s[i] - 10 + 'A';
			break;
		}
	}
	for (int i = 32 - p; i <= 32; i++)
		s[i] = '\0';
	std::string rs(s);
	delete[]s;
	return rs;
}

bool QInt::FromDec(char* s)
{
	//Dua du lieu chuoi s vao chuoi BigIntChar
	//Kiem tra chuoi hop le hay khong
	//chuyen dang ky tu thanh dang so
	//Xac dinh so am hay duong
	int len = strlen(s);
	char BigIntChar[256] = { 0 };
	memcpy(BigIntChar, s, len);
	int SignInt = 0;
	if (BigIntChar[0] == '-') SignInt = 1;
	if (SignInt == len)	return false;
	for (int i = SignInt; i < len; i++)
	{
		if (BigIntChar[i] < '0' || BigIntChar[i] > '9')
			return false;
		BigIntChar[i] -= '0';
	}
	//Kiem tra do dai len <= 128 ky tu
	if (len > 128) return false;
	//Dua day so dang duoc luu tu dau mang ve cuoi mang
	for (int i = len - 1; i >= SignInt; i--)
		BigIntChar[i + 128 - len] = BigIntChar[i];
	for (int i = 0; i < SignInt + 128 - len; i++)
		BigIntChar[i] = 0;
	//Nen du lieu bang cach: 
	//	+	Nhom tung bo 8 chu so lien tiep nhau trong mang char
	//	+	Nhom lai thanh 1 day cac so co 8 chu so lien tiep nhau trong mang long long
	long long *BigIntLL = (long long *)BigIntChar;
	for (int i = _length - 1; i >= 0; i--)
	{
		long long t = 0;
		for (int j = 0; j < 8; j++)
			t = t * 10 + BigIntChar[8 * i + j];
		BigIntLL[i] = t;
	}
	//Tu 1 day cac so co 8 chu so lien tiep nhau trong mang long long
	//Div va mod cho 256 de dien gia tri tung byte trong mang QInt.data
	for (int i = 0; i < _length; i++)
	{
		long long r = 0;
		for (int j = 0; j < _length; j++)
		{
			BigIntLL[j] += r * 100000000;
			r = BigIntLL[j] % 256;
			BigIntLL[j] /= 256;
		}
		_storage[i] = r;
	}
	//Dao nguoc thu tu cua mang (CITs)
	for (int i = 0; i < _length/2; i++)
	{
		BasisType ttt = _storage[i];
		_storage[i] = _storage[_length - 1 - i];
		_storage[_length - 1 - i] = ttt;
	}
	//Neu la so am, lay so bu 2
	if (SignInt)
	{
		*this = TwosComplement();
	}
	return true;
}

std::string QInt::ToDec()
{
	//Dua du lieu chuoi QInt.data vao tempData
	//Kiem tra dau am hay duong
	//Neu am, lay so bu 2 de tra ve lai thanh so duong
	BasisType *tempData = new BasisType[_length];
	char *s = new char[129];
	int SignInt = 0;
	QInt tmp;
	if (IsLessThanZero())
	{
		SignInt = 1;
		tmp = TwosComplement();
		memcpy(tempData, tmp.GetStorage(), _length);
	}
	else
	{
		memcpy(tempData, _storage, _length);
	}
	//Dua tung gia tri bit trong tempData vao day cac so co 8 chu so trong mang long long
	memset(s, 0, 128);
	long long *BigIntLL = (long long *)s;
	for (int i = _length-1; i >= 0; i--)
	{
		long long q = tempData[_length -1 -i];
		if (q < 0) q += 256;
		for (int j = 15; j >= 0; j--)
		{
			BigIntLL[j] = 256 * BigIntLL[j] + q;
			q = BigIntLL[j] / 100000000;
			BigIntLL[j] %= 100000000;
		}
	}
	//Tach day cac so co 8 chu so thanh 1 day cac chu so roi rac
	//Day chu so roi rac chinh la bieu dien thap phan cua QInt
	for (int i = 0; i < _length; i++)
	{
		long long t = BigIntLL[i];
		for (int j = 7; j >= 0; j--)
		{
			s[8 * i + j] = t % 10;
			t /= 10;
		}
	}
	//Chuyen nhung gia tri co nghia o cuoi chuoi len dau chuoi
	//Chuyen dang so thanh dang ky tu
	//Neu la so am, them dau tru dang truoc
	int p;
	for (p = 0; p < 127; p++)
		if (s[p] > 0) break;
	for (int i = p; i < 128; i++)
		s[i + SignInt - p] = s[i] + '0';
	for (int i = 128 + SignInt - p; i <= 128; i++)
		s[i] = '\0';
	if (SignInt) s[0] = '-';

	std::string rs(s);
	delete[]s;
	delete[]tempData;
	return rs;
}

bool QInt::FromBin(char * s)
{
	//Dua du lieu chuoi s vao chuoi BigIntChar
	//Kiem tra chuoi hop le hay khong
	//Chuyen dang ky tu thanh dang so
	int len = strlen(s);
	char BigIntChar[256];
	memcpy(BigIntChar, s, len);
	for (int i = 0; i < len; i++)
	{
		if (BigIntChar[i] < '0' || BigIntChar[i] > '1')
			return false;
		BigIntChar[i] -= '0';
	}
	//Dua tung bit chuoi vao QInt.data sao cho dung vi tri
	for (int i = 0; i < _length; i++)
	{
		_storage[_length - 1 - i] = 0;
		len -= 8;
		for (int j = 0; j < 8; j++)
		{
			if (len + j < 0) continue;
			_storage[_length - 1 - i] = _storage[_length - 1 - i] * 2 + BigIntChar[len + j];
		}
	}
	return true;
}

std::string QInt::ToBin(bool __isFullBit)
{
	//Dua tung bit trong QInt.data vao chuoi s sao cho dung vi tri
	//Chuyen doi dang so thanh dang ky tu
	int len = 128;
	char *s = new char[129];
	s[len] = '\0';
	for (int i = 0; i < _length; i++)
	{
		int t = _storage[_length - 1 - i];
		if (t < 0) t += 256;
		len -= 8;
		for (int j = 7; j >= 0; j--)
		{
			s[len + j] = '0' + t % 2;
			t /= 2;
		}
	}
	//Chuyen nhung gia tri co nghia o cuoi chuoi len dau chuoi
	if (!__isFullBit)
	{
		int p;
		for (p = 0; p < 127; p++)
			if (s[p] == '1') break;
		for (int i = p; i < 128; i++)
			s[i - p] = s[i];
		for (int i = 128 - p; i <= 128; i++)
			s[i] = '\0';
	}
	std::string rs(s);
	delete[]s;
	return rs;
}

bool QInt::operator[](BasisType __a)
{
	return GetBitByPosition(__a);
}

bool QInt::GetBitByPosition(BasisType __a)
{
	if (__a < 0 || __a >= 128) return 0;
	//vi tri cua phan tu trong mang, tinh tu phai sang trai
	BasisType unitPosition = __a / (_basisSize * 8);
	BasisType tmp = _storage[_length - 1 - unitPosition];
	//vi tri cua bit trong phan tu, tinh tu phai sang trai
	BasisType bitPosition = __a - unitPosition * _basisSize;
	if (tmp & (1<<bitPosition))
	{
		return 1;
	}
	return 0;
}
