#pragma once
#include <iostream>
#include <string>
typedef unsigned char BasisType;
class QInt
{
private:
	BasisType* _storage;
	const BasisType _basisSize = sizeof(BasisType);
	const BasisType _length = 128/8/_basisSize;
public:
	//KhoiTao QInt=0
	QInt();
	//Khoi tao, can tao 1 vung nho rieng rong 128bit truoc khi truyen con tro vao
	QInt(BasisType* __a);
	//Ham huy, giai phong bo nho
	~QInt();
	//Ham kiem tra QInt < 0
	bool IsLessThanZero();
	//Cac phep toan co ban tren bit
	QInt operator|(const QInt &__a);
	QInt operator&(const QInt &__a);
	QInt operator^(const QInt &__a);
	QInt operator!();
	QInt operator~();

	//Cac phep dich bit luan li
	QInt operator<<(BasisType __a);
	QInt operator>>(BasisType __a);
	//Phep gan
	QInt& operator=(QInt &__a);
	//Cac phep so sanh
	bool operator>(QInt &__a);
	bool operator>=(QInt &__a);
	bool operator<(QInt &__a);
	bool operator<=(QInt &__a);
	bool operator==(QInt &__a);
	bool operator!=(QInt &__a);
	//so bu 2
	QInt TwosComplement();
	//Cac phep dich bit so hoc
	QInt ShiftRightArithmetic(BasisType __a);
	QInt ShiftLeftArithmetic(BasisType __a);
	//Cac phep xoay bit
	void RolloverLeft();
	void RolloverRight();
	//lay gia tri cua bit theo vi tri tu phai sang trai (ho tro phep nhan)
	bool GetBitByPosition(BasisType __a);
	bool operator[](BasisType __a);
	//Cac phep toan so hoc
	QInt operator+(const QInt &__a);
	QInt operator-(QInt &__a);
	QInt operator*(QInt &__a);
	QInt operator/(QInt &__a);
	QInt operator%(QInt &__a);
	//Chuyen doi cac he co so
	bool FromHex(char* s);
	std::string ToHex();
	bool FromDec(char* s);
	std::string ToDec();
	bool FromBin(char* s);
	std::string ToBin(bool __isFullBit);
	//Cac ham khac
	BasisType* GetStorage();
};

