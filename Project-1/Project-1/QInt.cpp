#include "string.h"
#include "QInt.h"
#include "stdio.h"

QInt::QInt()
{
	memset(data, 0, 16);
}

//Ham nhap so nguyen lon tu thiet bi nhap chuan
void QInt::ScanQInt()
{
	char s[256] = { 0 };
	scanf_s("%s", s);
	//Goi ham chuyen so thap phan thanh QInt
	DecToQInt(s);
}

//Ham chuyen so nhi phan thanh QInt
void QInt::BinToQInt(char *s)
{
	//Dua du lieu chuoi s vao chuoi BigIntChar
	//Kiem tra chuoi hop le hay khong
	//Chuyen dang ky tu thanh dang so
	int len = strlen(s);
	char BigIntChar[256];
	memcpy(BigIntChar, s, len);
	for (int i = 0; i < len; i++)
	{
		if (BigIntChar[i] < '0' || BigIntChar[i] > '1')
			return;
		BigIntChar[i] -= '0';
	}
	//Dua tung bit chuoi vao QInt.data sao cho dung vi tri
	for (int i = 0; i < 16; i++)
	{
		data[i] = 0;
		len -= 8;
		for (int j = 0; j < 8; j++)
		{
			if (len + j < 0) continue;
			data[i] = data[i] * 2 + BigIntChar[len + j];
		}
	}
}

//Ham chuyen doi so thap phan thanh QInt
void QInt::DecToQInt(char *s)
{
	//Dua du lieu chuoi s vao chuoi BigIntChar
	//Kiem tra chuoi hop le hay khong
	//chuyen dang ky tu thanh dang so
	//Xac dinh so am hay duong
	int len = strlen(s);
	char BigIntChar[256] = { 0 };
	memcpy(BigIntChar, s, len);
	int SignInt = 0;
	if (BigIntChar[0] == '-') SignInt = 1;
	if (SignInt == len)	return;
	for (int i = SignInt; i < len; i++)
	{
		if (BigIntChar[i] < '0' || BigIntChar[i] > '9')
			return;
		BigIntChar[i] -= '0';
	}
	//Kiem tra do dai len <= 128 ky tu
	if (len > 128) return;
	//Dua day so dang duoc luu tu dau mang ve cuoi mang
	for (int i = len - 1; i >= SignInt; i--)
		BigIntChar[i + 128 - len] = BigIntChar[i];
	for (int i = 0; i < SignInt + 128 - len; i++)
		BigIntChar[i] = 0;
	//Nen du lieu bang cach: 
	//	+	Nhom tung bo 8 chu so lien tiep nhau trong mang char
	//	+	Nhom lai thanh 1 day cac so co 8 chu so lien tiep nhau trong mang long long
	long long *BigIntLL = (long long *)BigIntChar;
	for (int i = 16 - 1; i >= 0; i--)
	{
		long long t = 0;
		for (int j = 0; j < 8; j++)
			t = t * 10 + BigIntChar[8 * i + j];
		BigIntLL[i] = t;
	}
	//Tu 1 day cac so co 8 chu so lien tiep nhau trong mang long long
	//Div va mod cho 256 de dien gia tri tung byte trong mang QInt.data
	for (int i = 0; i < 16; i++)
	{
		long long r = 0;
		for (int j = 0; j < 16; j++)
		{
			BigIntLL[j] += r * 100000000;
			r = BigIntLL[j] % 256;
			BigIntLL[j] /= 256;
		}
		data[i] = r;
	}
	//Neu la so am, lay so bu 2
	if (SignInt)
	{
		for (int i = 0; i < 16; i++)
			data[i] = ~(data[i]);
		for (int i = 0; i < 16; i++)
		{
			if (data[i] != -1)
			{
				data[i]++;
				break;
			}
			data[i] = 0;
		}
	}
}

//Ham chuyen do so thap luc phan thanh QInt
void QInt::HexToQInt(char *s)
{
	//Dua du lieu chuoi s vao chuoi BigIntChar
	//Kiem tra chuoi hop le hay khong
	//chuyen dang ky tu thanh dang so
	int len = strlen(s);
	char BigIntChar[256];
	memcpy(BigIntChar, s, len);
	for (int i = 0; i < len; i++)
	{
		switch (BigIntChar[i])
		{
		case '0': case '1': case '2': case '3': case '4': case '5': case '6': case '7': case '8': case '9':
			BigIntChar[i] -= '0';
			break;
		case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
			BigIntChar[i] += -'A' + 10;
			break;
		case 'a': case 'b': case 'c': case 'd': case 'e': case 'f':
			BigIntChar[i] += -'a' + 10;
			break;
		default:
			return;
		}
	}
	//Dua tung gia tri thap luc phan vao QInt.data sao cho dung vi tri
	for (int i = 0; i < 16; i++)
	{
		data[i] = 0;
		len -= 2;
		for (int j = 0; j < 2; j++)
		{
			if (len + j < 0) continue;
			data[i] = 16 * data[i] + BigIntChar[len + j];
		}
	}
}

//Ham xuat so nguyen lon tu thiet bi xuat chuan
void QInt::PrintQInt() const
{
	char s[256] = { 0 };
	//Goi ham chuyen QInt thanh so thap phan
	QIntToDec(s);
	printf("%s", s);
}

//Ham chuyen QInt thanh so nhi phan
void QInt::QIntToBin(char *s) const
{
	//Dua tung bit trong QInt.data vao chuoi s sao cho dung vi tri
	//Chuyen doi dang so thanh dang ky tu
	int len = 128;
	s[len] = '\0';
	for (int i = 0; i < 16; i++)
	{
		int t = data[i];
		if (t < 0) t += 256;
		len -= 8;
		for (int j = 7; j >= 0; j--)
		{
			s[len + j] = '0' + t % 2;
			t /= 2;
		}
	}
	//Chuyen nhung gia tri co nghia o cuoi chuoi len dau chuoi
	int p;
	for (p = 0; p < 127; p++)
	if (s[p] == '1') break;
	for (int i = p; i < 128; i++)
		s[i - p] = s[i];
	for (int i = 128 - p; i <= 128; i++)
		s[i] = '\0';
}

//Ham chuyen QInt thanh so thap phan
void QInt::QIntToDec(char *s) const
{
	//Dua du lieu chuoi QInt.data vao tempData
	//Kiem tra dau am hay duong
	//Neu am, lay so bu 2 de tra ve lai thanh so duong
	char tempData[16];
	memcpy(tempData, data, 16);
	int SignInt = 0;
	if (tempData[15] < 0) SignInt = 1;
	if (SignInt)
	{
		for (int i = 0; i < 16; i++)
			tempData[i] = ~(tempData[i]);
		for (int i = 0; i < 16; i++)
		{
			if (tempData[i] != -1)
			{
				tempData[i]++;
				break;
			}
			tempData[i] = 0;
		}
	}
	//Dua tung gia tri bit trong tempData vao day cac so co 8 chu so trong mang long long
	memset(s, 0, 128);
	long long *BigIntLL = (long long *)s;
	for (int i = 15; i >= 0; i--)
	{
		long long q = tempData[i];
		if (q < 0) q += 256;
		for (int j = 15; j >= 0; j--)
		{
			BigIntLL[j] = 256 * BigIntLL[j] + q;
			q = BigIntLL[j] / 100000000;
			BigIntLL[j] %= 100000000;
		}
	}
	//Tach day cac so co 8 chu so thanh 1 day cac chu so roi rac
	//Day chu so roi rac chinh la bieu dien thap phan cua QInt
	for (int i = 0; i < 16; i++)
	{
		long long t = BigIntLL[i];
		for (int j = 7; j >= 0; j--)
		{
			s[8 * i + j] = t % 10;
			t /= 10;
		}
	}
	//Chuyen nhung gia tri co nghia o cuoi chuoi len dau chuoi
	//Chuyen dang so thanh dang ky tu
	//Neu la so am, them dau tru dang truoc
	int p;
	for (p = 0; p < 127; p++)
	if (s[p] > 0) break;
	for (int i = p; i < 128; i++)
		s[i + SignInt - p] = s[i] + '0';
	for (int i = 128 + SignInt - p; i <= 128; i++)
		s[i] = '\0';
	if (SignInt) s[0] = '-';
}

//Ham chuyen QInt thanh so thap luc phan
void QInt::QIntToHex(char *s) const
{
	//Dua tung gia tri thap luc phan trong QInt.data vao chuoi s sao cho dung vi tri
	int len = 32;
	s[len] = '\0';
	for (int i = 0; i < 16; i++)
	{
		int t = data[i];
		if (t < 0) t += 256;
		len -= 2;
		for (int j = 1; j >= 0; j--)
		{
			s[len + j] = t % 16;
			t /= 16;
		}
	}
	//Chuyen nhung gia tri co nghia o cuoi chuoi len dau chuoi
	//Chuyen doi dang so thanh dang ky tu
	int p;
	for (p = 0; p < 31; p++)
	if (s[p] > 0) break;
	for (int i = p; i < 32; i++)
	{
		switch (s[i])
		{
		case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: case 9:
			s[i - p] = s[i] + '0';
			break;
		case 10: case 11: case 12: case 13: case 14: case 15:
			s[i - p] = s[i] - 10 + 'A';
			break;
		}
	}
	for (int i = 32 - p; i <= 32; i++)
		s[i] = '\0';
}

//Ham cong 2 so nguyen
QInt QInt::operator + (const QInt &b) const
{
	QInt s;
	int q = 0;
	for (int i = 0; i < 16; i++)
	{
		//Cong tuong ung tung byte voi tung byte
		//q duoc dung lam carry de nho cong tiep vao byte tiep theo
		q += data[i] + ((data[i] < 0) ? 256 : 0);
		q += b.data[i] + ((b.data[i] < 0) ? 256 : 0);
		s.data[i] = q % 256;
		q /= 256;
	}
	//Tra ra gia tri s la tong cua 2 so nguyen
	return s;
}

//Ham tru 2 so nguyen
QInt QInt::operator - (const QInt &b) const
{
	//Tao bTemp la so bu 2 cua b
	QInt bTemp = b;
	for (int i = 0; i < 16; i++)
		bTemp.data[i] = ~(bTemp.data[i]);
	for (int i = 0; i < 16; i++)
	{
		if (bTemp.data[i] != -1)
		{
			bTemp.data[i]++;
			break;
		}
		bTemp.data[i] = 0;
	}
	//Tra ra ket qua phep tru la phep cong voi so bu 2
	return (*this) + bTemp;
}

//Ham nhan 2 so nguyen
QInt QInt::operator * (const QInt &b) const
{
	//Tao aTemp va bTemp luu ban sao gia tri cua 2 so nguyen
	//SignInt duoc dung de xac dinh dau cua ket qua tich 2 so nguyen
	QInt p, aTemp = (*this), bTemp = b;
	int SignInt = 1;
	//Neu aTemp la so am thi lay bu 2
	if (aTemp.data[15] < 0)
	{
		SignInt *= -1;
		for (int i = 0; i < 16; i++)
			aTemp.data[i] = ~(aTemp.data[i]);
		for (int i = 0; i < 16; i++)
		{
			if (aTemp.data[i] != -1)
			{
				aTemp.data[i]++;
				break;
			}
			aTemp.data[i] = 0;
		}
	}
	//Neu bTemp la so am thi lay bu 2
	if (bTemp.data[15] < 0)
	{
		SignInt *= -1;
		for (int i = 0; i < 16; i++)
			bTemp.data[i] = ~(bTemp.data[i]);
		for (int i = 0; i < 16; i++)
		{
			if (bTemp.data[i] != -1)
			{
				bTemp.data[i]++;
				break;
			}
			bTemp.data[i] = 0;
		}
	}
	//Thuc hien phep nhan giua aTemp va bTemp
	long long pTemp[33] = { 0 };
	for (int i = 0; i < 16; i++)
	{
		for (int j = 0; j < 16; j++)
		{
			int x = aTemp.data[i] + ((aTemp.data[i] < 0) ? 256 : 0);
			int y = bTemp.data[j] + ((bTemp.data[j] < 0) ? 256 : 0);
			pTemp[i + j] += x*y;
		}
	}
	long long q = 0;
	for (int i = 0; i < 16; i++)
	{
		pTemp[i] += q;
		q = pTemp[i] / 256;
		pTemp[i] %= 256;
		p.data[i] = pTemp[i];
	}
	//Neu ket qua la so am thi lay bu 2 cua p
	if (SignInt < 0)
	{
		for (int i = 0; i < 16; i++)
			p.data[i] = ~(p.data[i]);
		for (int i = 0; i < 16; i++)
		{
			if (p.data[i] != -1)
			{
				p.data[i]++;
				break;
			}
			p.data[i] = 0;
		}
	}
	//Tra ra ket qua p la tich cua 2 so nguyen
	return p;
}

//Ham chia 2 so nguyen
QInt QInt::operator / (const QInt &b) const
{
	QInt q, temp, aTemp = (*this), bTemp = b;
	//Kiem tra so chia co bang 0 hay khong
	int isZero = 1;
	for (int i = 0; i < 16; i++)
	if (bTemp.data[i] != 0)
	{
		isZero = 0;
		break;
	}
	if (isZero) return q;
	//Tao aTemp va bTemp luu ban sao gia tri cua 2 so nguyen
	//SignInt duoc dung de xac dinh dau cua ket qua tich 2 so nguyen
	//Neu aTemp la so am thi lay bu 2
	//Neu bTemp la so am thi lay bu 2
	int SignInt = 1;
	if (aTemp.data[15] < 0)
	{
		SignInt *= -1;
		for (int i = 0; i < 16; i++)
			aTemp.data[i] = ~(aTemp.data[i]);
		for (int i = 0; i < 16; i++)
		{
			if (aTemp.data[i] != -1)
			{
				aTemp.data[i]++;
				break;
			}
			aTemp.data[i] = 0;
		}
	}
	if (bTemp.data[15] < 0)
	{
		SignInt *= -1;
		for (int i = 0; i < 16; i++)
			bTemp.data[i] = ~(bTemp.data[i]);
		for (int i = 0; i < 16; i++)
		{
			if (bTemp.data[i] != -1)
			{
				bTemp.data[i]++;
				break;
			}
			bTemp.data[i] = 0;
		}
	}
	//Tim byte cao nhat cua bTemp khac 0
	//De tu do xac dinh byte cao nhat cua q khac 0
	int p;
	for (p = 15; bTemp.data[p] == 0; p--);
	//Lan luot tim gia tri phu hop cho tung byte cua thuong
	//Dam bao a >= b * q voi q la so nguyen duong lon nhat
	//Dong thoi dam bao b * q khong bi tran so dan den ket qua sai
	for (int i = 15 - p; i >= 0; i--)
	{
		int j;
		for (j = 0; j < 256; j++)
		{
			q.data[i] = j;
			temp = bTemp * q;
			int tst = 0;
			for (int k = 15; k >= 0; k--)
			{
				int x = aTemp.data[k] + ((aTemp.data[k] < 0) ? 256 : 0);
				int y = temp.data[k] + ((temp.data[k] < 0) ? 256 : 0);
				if (x != y)
				{
					tst = (x < y) ? 1 : 0;
					break;
				}
			}
			if (tst) break;
		}
		q.data[i] = j - 1;
	}
	//Neu ket qua la so am thi lay bu 2 cua q
	if (SignInt < 0)
	{
		for (int i = 0; i < 16; i++)
			q.data[i] = ~(q.data[i]);
		for (int i = 0; i < 16; i++)
		{
			if (q.data[i] != -1)
			{
				q.data[i]++;
				break;
			}
			q.data[i] = 0;
		}
	}
	//Tra ra ket qua q la thuong cua phep chia 2 so nguyen
	return q;
}