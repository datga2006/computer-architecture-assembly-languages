﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace GiaoDienTinhToan
{
    class MainViewModel : INotifyPropertyChanged
    {
        private int CoSo1 = 10;
        private int CoSo2 = 10;
        public String SourceFile { get; set; }
        public String PhepToan { get; set; }
        public String KetQua { get; set; }
        public bool CheDoChuyenDoi { get; set; }
        public ICommand TinhToan
        {
            get; set;
        }

        public ICommand Dec1 { get; set; }
        public ICommand Hex1 { get; set; }
        public ICommand Bin1 { get; set; }
        public ICommand Dec2 { get; set; }
        public ICommand Hex2 { get; set; }
        public ICommand Bin2 { get; set; }
        public MainViewModel()
        {
            SourceFile = "";
            CheDoChuyenDoi = false;
            TinhToan = new RelayCommand<String>(
                (o) => PhepToan != "" && SourceFile!="",
                (o) => {
                    String arg = "";
                    if (CheDoChuyenDoi)
                    {
                        arg = arg + CoSo1 + " " + CoSo2 + " " + PhepToan;
                    }
                    else
                    {
                        arg = arg + CoSo1 + " " + PhepToan;
                    }
                    Process p = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            FileName = SourceFile,
                            Arguments = arg,
                            UseShellExecute = false,
                            RedirectStandardOutput = true,
                            CreateNoWindow = true
                        }
                    };
                    p.Start();
                    p.WaitForExit();
                    string output = p.StandardOutput.ReadToEnd();
                    KetQua = output;
                    OnPropertyChanged("KetQua");
                });
            Dec1 = new RelayCommand<String>(
                (o) => CoSo1 != 10,
                (o) => CoSo1 = 10);
            Hex1 = new RelayCommand<String>(
                (o) => CoSo1 != 16,
                (o) => CoSo1 = 16);
            Bin1 = new RelayCommand<String>(
                (o) => CoSo1 != 2,
                (o) => CoSo1 = 2);
            Dec2 = new RelayCommand<String>(
               (o) => CheDoChuyenDoi && CoSo2 != 10,
               (o) => CoSo2 = 10);
            Hex2 = new RelayCommand<String>(
                (o) => CheDoChuyenDoi && CoSo2 != 16,
                (o) => CoSo2 = 16);
            Bin2 = new RelayCommand<String>(
                (o) => CheDoChuyenDoi && CoSo2 != 2,
                (o) => CoSo2 = 2);
            PhepToan = "";
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
