#pragma once

#ifndef _QInt
#define _QInt

class QInt
{
private:
	char data[16];
public:
	QInt();

	void ScanQInt();
	void BinToQInt(char *);
	void DecToQInt(char *);
	void HexToQInt(char *);
	void PrintQInt() const;
	void QIntToBin(char *) const;
	void QIntToDec(char *) const;
	void QIntToHex(char *) const;
	QInt operator + (const QInt &) const;
	QInt operator - (const QInt &) const;
	QInt operator * (const QInt &) const;
	QInt operator / (const QInt &) const;
};

#endif 